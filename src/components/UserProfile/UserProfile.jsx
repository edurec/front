import React, { Component } from "react";

import styles from "./UserProfile.module.css";
import icon from "./Icon.jpg";

const mapRoleToLabel = {
  student: "Пользователь библиотеки"
};

const getRoleLabel = type => {
  if (mapRoleToLabel[type] === undefined) {
    return "Пользователь библиотеки";
  }

  return mapRoleToLabel[type];
};

class UserProfile extends Component {
  render() {
    const { userName = "" } = this.props;

    return (
      <div className={styles.container}>
        <div className={styles.info}>
          <span className={styles.name}>{userName}</span>
          <span className={styles.role}>{getRoleLabel()}</span>
        </div>
        <span>
          <img src={icon} alt="userpic" />
        </span>
      </div>
    );
  }
}

export default UserProfile;

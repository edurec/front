import React, { Component } from "react";
import classNames from "classnames";

import styles from "./IconButton.module.css";

class IconButton extends Component {
  render() {
    const { active, disabled, onClick, children } = this.props;

    const buttonClassNames = classNames(styles.button, {
      [styles.active]: active
    });

    return (
      <button
        type="button"
        className={buttonClassNames}
        disabled={disabled}
        onClick={onClick}
      >
        <div className={styles.icon}>{children}</div>
      </button>
    );
  }
}

export default IconButton;

import React, { Component } from "react";

import styles from "./CheckBox.module.css";

import { ReactComponent as Icon } from "./Icon.svg";

class CheckBox extends Component {
  render() {
    const { checked = true } = this.props;

    return <div className={styles.checkbox}>{checked && <Icon />}</div>;
  }
}

export default CheckBox;

import React, { Component } from "react";
import classNames from "classnames";

import styles from "./Card.module.css";

class BaseCard extends Component {
  getBadge() {
    const { subjectName } = this.props;

    const badgeClassName = classNames(styles.badge, {
      [styles.algebra]: subjectName === "Алгебра",
      [styles.physics]:
        subjectName === "Физика" || subjectName === "История России",
      [styles.russian]:
        subjectName === "Русский язык" || subjectName === "Английский язык"
    });

    return <div className={badgeClassName}>{subjectName}</div>;
  }

  getCategory() {
    const { category } = this.props;
    return <div className={styles.category}>{category}</div>;
  }

  getTitle() {
    const { title } = this.props;
    return (
      <div className={styles.title} title={title}>
        {title}
      </div>
    );
  }

  getRating() {
    const { rating } = this.props;

    if (rating === undefined) {
      return null;
    }

    return <div className={styles.rating}>Rating</div>;
  }

  getBacklog() {
    const { backlog } = this.props;

    if (backlog === undefined) {
      return null;
    }

    return <div className={styles.backlog}>{backlog}</div>;
  }

  render() {
    const { big, group, subjectName } = this.props;

    const cardClassName = classNames(styles.card, {
      [styles.big]: big,
      [styles.group]: group,
      [styles.algebra]: subjectName === "Алгебра",
      [styles.physics]:
        subjectName === "Физика" || subjectName === "История России",
      [styles.russian]:
        subjectName === "Русский язык" || subjectName === "Английский язык"
    });

    const badge = this.getBadge();
    const category = this.getCategory();
    const title = this.getTitle();
    const backlog = this.getBacklog();

    return (
      <div className={cardClassName}>
        {badge}
        <div className={styles.bottom}>
          {category}
          {title}
          {backlog}
        </div>
      </div>
    );
  }
}

export default BaseCard;

import React from "react";

import styles from "./Hello.module.css";

const Hello = ({ count }) => (
  <div className={styles.container}>
    Hello!
    <span>{count}</span>
  </div>
);

export default Hello;

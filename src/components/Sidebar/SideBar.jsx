import React, { Component } from "react";

import Filters from "../Filters";

import styles from "./SideBar.module.css";

class SideBar extends Component {
  render() {
    return (
      <div className={styles.sidebar}>
        <Filters />
      </div>
    );
  }
}

export default SideBar;

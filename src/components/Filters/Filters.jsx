import React, { Component } from "react";
// import classNames from "classnames";

import { CheckBox } from "../controls";

import styles from "./Filters.module.css";

const filtersLabel = "фильтры";
const resetLabel = "сбросить";

const filters = [
  {
    id: "all",
    label: "все",
    expandable: false
  },
  {
    id: "scenarios",
    label: "сценарии уроков",
    expandable: false
  },
  {
    id: "supplements",
    label: "приложения",
    expandable: true
  },
  {
    id: "tests",
    label: "тесты",
    expandable: false
  },
  {
    id: "fiction",
    label: "худ. литература",
    expandable: true
  },
  {
    id: "atomics",
    label: "атомики",
    expandable: true
  },
  {
    id: "tutorials",
    label: "учебные пособия",
    expandable: false
  }
];

// const educationLevelFilters = [
//   { id: "start", label: "ноо" },
//   { id: "middle", label: "ооо" },
//   { id: "high", label: "соо" }
// ];

// const studyLevelFilters = [
//   { id: "base", label: "базовый" },
//   { id: "extend", label: "углублённый" }
// ];

const FiltersHeader = ({ canReset = false, onReset = null }) => {
  return (
    <div className={styles.header}>
      <span className={styles.filtersLabel}>{filtersLabel}</span>
      <button className={styles.resetButton} disabled={!canReset}>
        {resetLabel}
      </button>
    </div>
  );
};

const FilterLine = ({ filter, handleChange, checked }) => {
  const { id, label, expandable } = filter;

  return (
    <div className={styles.filterLine}>
      <div className={styles.filterContent} onClick={() => handleChange(id)}>
        <CheckBox checked={checked} />
        <span className={styles.filterLabel}>{label}</span>
      </div>
      {expandable && <div className={styles.expand} />}
    </div>
  );
};

class Filters extends Component {
  constructor(props) {
    super(props);

    this.state = {
      all: true,
      scenarios: false,
      supplements: false,
      tests: false,
      fiction: false,
      atomics: false,
      tutorials: false,
      start: false,
      middle: false,
      high: false,
      base: false,
      extend: false
    };

    this.handleFilterChange = this.handleFilterChange.bind(this);
  }

  handleFilterChange(filterId) {
    const newState = {};
    Object.keys(this.state).forEach(key => {
      newState[key] = key === filterId;
    });

    this.setState(newState);
  }

  render() {
    const { handleFilterChange, state } = this;
    const canReset = !state.all;

    return (
      <div className={styles.container}>
        <FiltersHeader canReset={canReset} />
        {filters.map(filter => {
          const checked = state[filter.id];
          return (
            <FilterLine
              key={filter.id}
              filter={filter}
              handleChange={handleFilterChange}
              checked={checked}
            />
          );
        })}
      </div>
    );
  }
}

export default Filters;

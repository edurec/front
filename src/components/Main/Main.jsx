import React, { Component } from "react";

import Section from "../Section";

import styles from "./Main.module.css";

class Main extends Component {
  render() {
    const { onExpand, groups } = this.props;

    return (
      <div className={styles.main}>
        {groups.map(({ type, elements }) => (
          <Section
            type={type}
            elements={elements}
            onExpand={onExpand}
            key={type}
          />
        ))}
      </div>
    );
  }
}

export default Main;

import React, { Component } from "react";

import styles from "./Logo.module.css";
import { ReactComponent as LogoImage } from "./Logo.svg";

class Logo extends Component {
  render() {
    return (
      <span className={styles.logo}>
        <LogoImage />
      </span>
    );
  }
}

export default Logo;

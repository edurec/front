import React, { Component } from "react";

import styles from "./Section.module.css";

import Card from "../Card";

const getCommonCount = elements => {
  return elements.reduce((sum, element) => sum + element.lessons.length, 0);
};

const getTitle = (type, elements) => {
  if (type === "missedLessons") {
    return `Пропущенные занятия (${getCommonCount(elements)})`;
  }

  if (type === "badScoreLessons") {
    return "Моя успеваемость";
  }

  return "Some section";
};

const expandLabel = "Смотреть всё";

class Section extends Component {
  getExpandButton() {
    const {
      type,
      elements: { length },
      onExpand
    } = this.props;

    return length >= 5 ? (
      <div className={styles.expand} onClick={() => onExpand(type)}>
        {expandLabel}
      </div>
    ) : null;
  }

  renderMissedCarousel() {
    const { elements } = this.props;

    return elements.map(({ subjectName, lessons }) => {
      const cardTitle = lessons.length > 1 ? subjectName : lessons[0].theme;
      const backlog =
        lessons.length === 1
          ? "1 занятие"
          : lessons.length >= 2 || lessons.length <= 4
          ? `${lessons.length} занятия`
          : `${lessons.length} занятий`;
      const category = lessons[0].type === "lesson" ? "Уроки" : "Другое";

      return (
        <Card
          subjectName={subjectName}
          category={category}
          title={cardTitle}
          backlog={backlog}
        />
      );
    });
  }

  renderBadScoreCarousel() {
    const { elements } = this.props;

    console.log(elements);

    return elements.map(({ subjectName, theme, type }) => {
      const cardTitle = theme;
      const category = type === "lesson" ? "Уроки" : "Другое";

      return (
        <Card subjectName={subjectName} category={category} title={cardTitle} />
      );
    });
  }

  renderCarousel() {
    const { type } = this.props;

    if (type === "missedLessons") {
      return this.renderMissedCarousel();
    }

    if (type === "badScoreLessons") {
      return this.renderBadScoreCarousel();
    }
  }

  render() {
    const { type, elements } = this.props;
    const title = getTitle(type, elements);
    const expandButton = this.getExpandButton();

    return (
      <div className={styles.section}>
        <div className={styles.header}>
          <div className={styles.title}>{title}</div>
          {expandButton}
        </div>
        <div className={styles.carousel}>{this.renderCarousel()}</div>
      </div>
    );
  }
}

export default Section;

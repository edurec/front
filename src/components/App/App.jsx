import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Hello from "../../containers/Hello";
import Header from "../../containers/Header";
import Main from "../../containers/Main";
import SearchPanel from "../SearchPanel";
import SideBar from "../Sidebar";

import styles from "./App.module.css";

class App extends Component {
  async componentDidMount() {
    await this.props.start();
  }

  componentDidUpdate(prevProps, prevState) {
    const { isAuthenticated, loadMissedLessons, loadBadScores } = this.props;

    if (isAuthenticated) {
      loadMissedLessons();
      loadBadScores();
    }
  }

  render() {
    return (
      <div className={styles.page}>
        <Header />
        <SearchPanel />
        <div className={styles.content}>
          <SideBar />
          <div className={styles.mainContent}>
            <Router>
              <Route exact path="/" component={Main} />
              <Route exact path="/hello" component={Hello} />
            </Router>
          </div>
        </div>
      </div>
    );
  }
}

export default App;

import React, { Component } from "react";

import styles from "./Header.module.css";

import Logo from "../Logo";
import UserProfile from "../../containers/UserProfile";

class Header extends Component {
  render() {
    return (
      <header className={styles.header}>
        <Logo />
        <UserProfile />
      </header>
    );
  }
}

export default Header;

export const INCREMENT = "counter/INCREMENT";
export const DECREMENT = "counter/DECREMENT";

export const increment = () => {
  return dispatch => {
    dispatch({
      type: INCREMENT
    });
  };
};

export const decrement = () => {
  return dispatch => {
    dispatch({
      type: DECREMENT
    });
  };
};

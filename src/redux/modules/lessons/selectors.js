const parseMissedSubject = missedSubject => {
  const { subject_id, lessons } = missedSubject;
  let subjectName;
  const parsedMissedLessons = lessons.map(missedLesson => {
    const {
      data: {
        name,
        education_metadata: { material_education_areas },
        type: rawType
      }
    } = missedLesson;

    subjectName = material_education_areas[0].subject_name;

    const type = rawType === "LessonTemplate" ? "lesson" : "unknown";

    return {
      type,
      theme: name
    };
  });

  return {
    subjectId: subject_id,
    subjectName,
    lessons: parsedMissedLessons
  };
};

const parseMissedLessons = missedLessons => {
  return missedLessons.map(parseMissedSubject);
};

const parseBadScoreLessons = badScoreLessons => {
  const reducer = (result, { lessons }) => {
    return [
      ...result,
      ...lessons.map(lesson => {
        const {
          data: {
            type: rawType,
            name: theme,
            education_metadata: { material_education_areas }
          }
        } = lesson;

        const subjectName = material_education_areas[0].subject_name;
        const type = rawType === "LessonTemplate" ? "lesson" : "unknown";

        return {
          subjectName,
          theme,
          type
        };
      })
    ];
  };

  return badScoreLessons.reduce(reducer, []);
};

// count
// subject name/id
// subject type

// theme

export { parseMissedLessons, parseBadScoreLessons };

import reducer from "./reducer";

import { loadMissedLessons, loadBadScores } from "./actions";

export { reducer as default, loadMissedLessons, loadBadScores };

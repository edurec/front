import { apiUrl } from "../../../config";

const REQUEST_MISSED_LESSONS = "LESSONS/REQUEST_MISSED_LESSONS";
const RECEIVE_MISSED_LESSONS = "LESSONS/RECEIVE_MISSED_LESSONS";

const loadMissedLessons = subjectId => async (dispatch, getState) => {
  const subjectString =
    subjectId !== undefined ? `&sunbject_id=${subjectId}` : "";
  const url = `${apiUrl}/missed-lessons?user_id=123${subjectString}`;
  const response = await fetch(url);
  const res = await response.json();

  dispatch({
    type: RECEIVE_MISSED_LESSONS,
    lessons: res.scores
  });
};

const RECEIVE_BAD_SCORE_LESSONS = "LESSONS/RECEIVE_BAD_SCORE_LESSONS";
const loadBadScores = subjectId => async (dispatch, getState) => {
  const subjectString =
    subjectId !== undefined ? `&sunbject_id=${subjectId}` : "";
  const url = `${apiUrl}/bad-score-lessons?user_id=123${subjectString}`;
  const response = await fetch(url);
  const res = await response.json();

  console.log(res);
  dispatch({
    type: RECEIVE_BAD_SCORE_LESSONS,
    lessons: res.scores
  });
};

export {
  REQUEST_MISSED_LESSONS,
  RECEIVE_MISSED_LESSONS,
  RECEIVE_BAD_SCORE_LESSONS,
  loadMissedLessons,
  loadBadScores
};

import { RECEIVE_MISSED_LESSONS, RECEIVE_BAD_SCORE_LESSONS } from "./actions";
import { parseMissedLessons, parseBadScoreLessons } from "./selectors";

const initialState = {
  topLessons: [],
  missedLessons: [],
  badScoreLessons: [],
  preparingMaterials: [],
  subjects: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case RECEIVE_MISSED_LESSONS: {
      const newState = {
        ...state,
        missedLessons: parseMissedLessons(action.lessons)
      };

      console.log(newState);

      return newState;
    }

    case RECEIVE_BAD_SCORE_LESSONS: {
      const newState = {
        ...state,
        badScoreLessons: parseBadScoreLessons(action.lessons)
      };

      console.log(newState);

      return newState;
    }

    default:
      return state;
  }
};

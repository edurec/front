const initialState = {};

export const reducerName = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

import fetch from "isomorphic-fetch";

import { apiUrl } from "../../../config";

const REQUEST_LOGIN = "USER/REQUEST_LOGIN";
const RECEIVE_LOGIN = "USER/RECEIVE_LOGIN";
const login = () => async (dispatch, getState) => {
  const url = `${apiUrl}/login?user_id=123`;
  const response = await fetch(url);
  const res = await response.json();

  await dispatch({
    type: RECEIVE_LOGIN,
    user: res
  });
};

export { REQUEST_LOGIN, RECEIVE_LOGIN, login };

import { REQUEST_LOGIN, RECEIVE_LOGIN } from "./actions";

const initialState = {
  isLoginProcessed: false,
  isAuthenticated: false,
  userName: "",
  user_id: 0,
  type: "",
  averageScores: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_LOGIN: {
      return { ...state, isLoginProcessed: true };
    }

    case RECEIVE_LOGIN: {
      const {
        user: { type, name: userName, avg_scores: averageScores, user_id }
      } = action;

      return {
        ...state,
        type,
        userName,
        averageScores,
        user_id,
        isAuthenticated: true
      };
    }

    default:
      return state;
  }
};

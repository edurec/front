import { combineReducers } from "redux";

import example from "./modules/example";
import user from "./modules/user";
import lessons from "./modules/lessons";

export default combineReducers({
  example,
  user,
  lessons
});

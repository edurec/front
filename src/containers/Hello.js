import { connect } from "react-redux";

import Hello from "../components/Hello";

const mapStateToProps = ({ example: { count } }) => ({
  count
});

export default connect(mapStateToProps)(Hello);

import { connect } from "react-redux";

import UserProfile from "../components/UserProfile";

const mapStateToProps = ({ user: { userName, type } }) => ({
  userName,
  type
});

export default connect(mapStateToProps)(UserProfile);

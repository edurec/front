import { connect } from "react-redux";

import Header from "../components/Header";

import { login } from "../redux/modules/user";

const mapDispatchToProps = dispatch => ({
  onClick: () => dispatch(login("ololosh", "fakepass"))
});

export default connect(
  null,
  mapDispatchToProps
)(Header);

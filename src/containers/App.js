import { connect } from "react-redux";

import App from "../components/App";

import { login } from "../redux/modules/user";
import { loadMissedLessons, loadBadScores } from "../redux/modules/lessons";

const mapStateToProps = ({ user: { isAuthenticated } }) => ({
  isAuthenticated
});

const mapDispatchToProps = dispatch => ({
  start: () => dispatch(login()),
  loadMissedLessons: () => dispatch(loadMissedLessons()),
  loadBadScores: () => dispatch(loadBadScores())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

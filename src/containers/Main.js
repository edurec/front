import { connect } from "react-redux";

import Main from "../components/Main";

const mapStateToProps = ({ lessons }) => {
  const groups = [];

  if (lessons.missedLessons.length > 0) {
    groups.push({
      type: "missedLessons",
      elements: lessons.missedLessons
    });
  }

  if (lessons.badScoreLessons.length > 0) {
    groups.push({
      type: "badScoreLessons",
      elements: lessons.badScoreLessons
    });
    groups.push({
      type: "missedLessons",
      elements: lessons.missedLessons
    });
    groups.push({
      type: "badScoreLessons",
      elements: lessons.badScoreLessons
    });
  }

  return {
    onExpand: type => console.log(type),
    groups
  };
};

// const mapDispatchToProps = null;

export default connect(mapStateToProps)(Main);
